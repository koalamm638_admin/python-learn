
'''
    2020-03-25 23:39:54
    这是给大家做的第二次python培训.
    上次给大家讲了怎样安装python解释器，以及怎样编写一个最简单的python hello world程序。
    这次主要讲python语法.
'''

# 内置数据类型

# 整型
age = 20
print(age)
print(type(age))
print("小明今年:" + str(age) + "岁了")
print('----------------------------------------')

# 浮点数
rate = 0.49
print(rate)
print(type(rate))
print('----------------------------------------')

# 字符串
name = "python"
print(name)
print(type(name))
print("人生苦短，我用" + name)
print('----------------------------------------')

# 列表
fruits = ['苹果', '香蕉', '橘子', '西瓜']
#print(fruits)
#print(type(fruits))
#print(fruits[0])
#print(fruits[1])
#print(fruits[1:])
#print(fruits[:1])
#print(fruits[-1:])
#print(fruits[:-1])
#print(fruits[:])
#print(fruits[1:2])
#fruits[0] = '梨子'
#print(fruits)
#print(fruits[:])
#print('----------------------------------------')
#fruits.append('火龙果')
#print(fruits)
#fruits.insert(0, '芒果')
#print(fruits)
#fruits.pop()
#print(fruits)
#fruits.reverse()
#print(fruits)

# 元组
foods = ('拌面', '烤肉', '大盘鸡')
print(foods)
print(type(foods))
print(foods[0])
#foods[0] = '凉皮'
print('----------------------------------------')

# 字典
profile = {}
print(profile)
print(type(profile))
profile['name'] = 'chengwei'
profile['email'] = 'chengwei@hmccb.com'
profile['sex'] = 'man'
print(profile)
print(profile.keys())
print(profile.values())
print(profile['name'])
for item in profile.keys():
    print('[{0}]-->[{1}]'.format(item, profile[item]))


# 条件判断
price = 100
if price > 50:
    print("多了")
else:
    print("不多")

age = 30
if age <=6:
    print("小朋友")
elif age <= 18:
    print("青少年")
else:
    print("成年人")


# for循环
foods = ['拌面', '烤肉', '大盘鸡']
for item in foods:
    print(item)

# for循环遍历range迭代器
for i in range(10):
    print(i)

# while循环
while len(foods) > 0:
    print(foods[-1:])
    foods.pop()

# while循环 模拟do-while 至少执行一次
count = 0
while True:
    print("hahaha")
    count += 1
    if count > 3:
        break




#def func2(name):
#    print("语言:" + name)
#
#if __name__ == '__main__':
#    func2("python")
#    #func1(host='localhost', port=3306, username='root', passwd='123456', db='test')


#def func1(**kwargs):
#    print(kwargs)
#    print(type(kwargs))
#    for key in kwargs:
#        print('[%s]-->[%s]' % (key, kwargs[key]))
#
#if __name__ == '__main__':
#    func1(host='localhost', port=3306, username='root', passwd='123456', db='test')


import os
os.chdir("D:\\")
files = os.listdir('.')
print(files)
