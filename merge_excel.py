import xlrd, os, sys
from xlutils.copy import copy

def parse_excel(filename):
    merge_book = xlrd.open_workbook(filename)
    write_book = copy(merge_book)
    files = [ x for x in os.listdir('.') if x.find("xlsname") > 1 ]
    global_row_1 = 1
    global_row_2 = 1
    for i in range(len(files)):
        date = files[i].split(".")[0][-8:]
        #print date
        read_book = xlrd.open_workbook(files[i])
        read_sheets = read_book.sheets()
        read_sheet_1 = read_sheets[0]
        read_sheet_2 = read_sheets[1]
        #print f

        write_sheet_1 = write_book.get_sheet(0)
        write_sheet_2 = write_book.get_sheet(1)

        for row in range(read_sheet_1.nrows):
            if i != 0 and row == 0:continue # skip table first line
            for col in range(8):
                value = read_sheet_1.cell_value(row, col)
                write_sheet_1.write(global_row_1, col, value)
            global_row_1 += 1

        for row in range(read_sheet_2.nrows):
            if i != 0 and row == 0:continue # skip table first line
            for col in range(8):
                value = read_sheet_2.cell_value(row, col)
                write_sheet_2.write(global_row_2, col, value)
            global_row_2 += 1
    write_book.save("temp.xls")
    
if __name__ == '__main__':
    if len(sys.argv) != 2:
        print "usage: python %s <ms-xls-file>" % sys.argv[0]
        sys.exit(-1)
    filename = sys.argv[1].strip()
    parse_excel(filename)
