import os
import sys

'''
    if use in windows, pls download https://ffmpeg.zeranoe.com/builds/ & install & add to PATH
'''

def flv2mp4(flvfile):
    filename = flvfile.split('.')[0]
    suffix = '.mp4'
    outname = filename + suffix
    command = 'ffmpeg -i {} {}'.format(flvfile, outname)
    print(command)
    os.system(command)

if __name__ == '__main__':
    flv2mp4(sys.argv[1])
