from concurrent.futures import ThreadPoolExecutor, as_completed
import time
import random

pages = [x for x in range(1,20)]

def foo(args):
    print(args)
    return args

executor = ThreadPoolExecutor(max_workers=10)
tasks = [executor.submit(foo, pagenum) for pagenum in pages]


for future in as_completed(tasks):
    ret = future.result()
    print(ret)
