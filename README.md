## 本仓库的意义

本仓库记录了我学习Python过程中编写的大部分代码，创建之初是想将编写的Python代码收集起来，防止丢失和重复编写。大部分代码，都是为了解决工作中遇到的实际问题。

最近几年，随着大数据、人工智能的火热，使用Python编程的小伙伴越来越多。我有幸在多年前接触到Python，知道一点皮毛。身边的同事想学Python，但无从下手。我就把这个仓库的代码和文档完善完善，让它发挥点价值吧。

## 我是如何接触Python的

我第一次接触Python，当时刚毕业，来到一家公司，公司里有一个技术很好的前辈，我听到他和其他同事讨论Python语言。我以前只会C/C++，第一次听说大蟒蛇语言，充满了好奇，就去搜索了一下，我还请教他，看他怎么编写的，后来问他要了一些他写过的代码，拿来学习。前辈也很不错，他喜欢别人和他探讨技术问题，慢慢地我也了解了一点Python的知识。

再后来，我发现用Python处理工作中的一些需求还是挺方便的，写起来很带劲，用很少的代码就能实现其他语言要写一大堆的功能。而且Python的语法非常优雅，就好像是在写伪代码，伪代码写好了，功能也就实现了，这种感觉非常棒，如果是一个新手，你会觉得很有满足感。在这个过程中，我陆续用Python编写过文本处理、文件解析、TCP Socket收发请求、发送监控邮件、excel处理、网站爬虫等功能。


## 如何安装Python

如果是Windows操作系统，从官网下载最新的Python二进制安装包。
Python官网下载地址：[https://www.python.org/downloads](https://www.python.org/downloads/)

安装完成后,如果安装路径是
```
D:\Python37
```
则把
```
D:\Python37
D:\Python37\Script
```
添加到环境变量PATH中即可.重新打开一个cmd窗口,输入*python* ,你就能看到Python解释器了。
输入
```
print('hello, Python!')
```
这是你的第一行Python代码。

如果是Mac或者Linux，自带Python，无需安装。

## 我该用什么IDE

带我入门Python的那位前辈，我以前问他，我该用什么IDE编写Python代码呀？他说，用一个你最顺手的文本编辑器就可以。
这么多年过去了，我也能理解他说的话了。最简单的反而是最实用的。
因为Python本身就是一门很简单的语言，只需一个Python解释器就可以逐行执行代码了。再复杂点，就打开一个文本编辑器，把代码写进去，保存成文件，执行：

```
python yourcode.py
```
现在的小伙伴，大部分喜欢用PyCharm，我不是很建议这么做，因为80%的情况下，只需要Python解释器+文本编辑器就够了。Pycharm里的一堆参数配置、解释器配置等等，很容易把一个新手搞晕。
如果是新手，建议使用vscode编写Python代码。

## 我该用Python2还是Python3
**Python3，毋庸置疑。**

不是说版本越高越好，而是因为Python2和Python3的差别太大了。几年前用Python2写代码， 经常要处理中文编码的问题。用Python3后，整个世界都变好了。

现在用Python2的场景很少，可能有时候刷CTF题，一些比较老的代码是用Python2写的，不得已才要用Python2。

## 平时工作如何编写Python
我一般会打开一个Python解释器，就是那个黑框框，在这个里面先验证一下我写的代码对不对，因为是一行行执行的，如果有报错，会立马看到错在哪里.

然后我会打开我心爱的编辑器gvim，将我调试好的Python代码保存在一个文本文件中,然后打开一个cmd窗口, cd到代码所在目录,*python yourcode.py*运行它。
就是这么简单， so easy~

如果运行程序文件报错时，如果需要调试，可以这样:
```
python -m pdb yourcode.py
```
这样会进行debug模式,逐行执行, 输入n回车,执行下一行.如需打印变量, 输入p var.
参考:
[Python 代码调试技巧](https://www.ibm.com/developerworks/cn/linux/l-cn-pythondebugger/)

## 忘记某个函数的用法怎么办
如何知道某个函数的用法，如何知道某个包下面有哪些类和函数，如何知道一个对象的类型，如何知道某个对象有哪些方法。
打开Python解释器：
```
>>> name = {}
>>> type(name)
<class 'dict'>
>>> help(name)
Help on dict object:

class dict(object)
 |  dict() -> new empty dictionary
 |  dict(mapping) -> new dictionary initialized from a mapping object's
 |      (key, value) pairs
 |  dict(iterable) -> new dictionary initialized as if via:

>>> import os
>>> dir(os)
['DirEntry', 'F_OK', 'MutableMapping', 'O_APPEND', 'O_BINARY', 'O_CREAT', 'O_EXCL', 'O_NOINHERIT', 'O_RANDOM', 'O_RDONLY', 'O_RDWR', 'O_SEQUENTIAL', 'O_SHORT_LIVED', 'O_TEMPORARY', 'O_TEXT', 'O_TRUNC', 'O_WRONLY', 'P_DETACH', 'P_NOWAIT', 'P_NOWAITO', 'P_OVERLAY', 'P_WAIT', 'PathLike', 'R_OK', 'SEEK_CUR', 'SEEK_END', 'SEEK_SET', 'TMP_MAX', 'W_OK', 'X_OK', '_Environ', '__all__', ...]

```
学会这个技巧后，基本不用查找第三方资料了。

总结一下，主要有这么几个有用的函数：

help: 查看函数或者对象的帮助文档

type: 查看对象的类型

dir: 查看包下有哪些函数和类


## 如何安装第三方库
全世界的程序员，为Python编写了很多功能强大的库，大部分基础性的功能，都有人帮你写好了。

你要做的就是像搭积木一样，把他们组装起来。
你可以用pip安装这些库，很简单。还记得上文提到我们把Python安装路径下的Script添加到环境变量里了吗？ pip就在那里面。 打开一个cmd窗口，输入pip回车，就能看到。

以安装requests库举例：
```
pip install requests
```
安装完成后，打开一个Python解释器，输入：
```
>>> import requests
>>> resp = requests.get('https://gitee.com/mktime')
>>> resp.text
```
这样就获取到了一个网址的html内容。

## 如何提高pip的下载速度
由于通过官方的pip源下载很慢，我们可以使用国内的镜像。
```
pip config set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple
```
这样就可以了。
详细设置请看这里：
[pypi 镜像使用帮助](https://mirrors.tuna.tsinghua.edu.cn/help/pypi/)

## 如何进阶
现在回头看几年前自己写的Python代码，内心就一个字：丑的一批。

所以我认为Python进阶，也就是如何将Python代码写得更优雅和更高效。

这里我总结了几个技巧，分享给大家。如果读者有比较优雅的写法，欢迎留言给我，我会补充进来。

###  字符串处理相关
字符串处理是写程序遇到的最多的一个场景，如果是以前，我可能会这样写:
```
>>> name = 'john'
>>> age = 27
>>> content = 'Hello, my name is ' + name + ',  I am ' + str(age) + '.'
>>> print(content)
Hello, my name is john,  I am 27.
>>>
```

上面这种写法，用了大量字符串拼接，一个是容易写错，而且遇到类型不匹配还需要自己转换。

使用字符串的format方法后，会优雅很多：
```
>>> content = 'Hello, My name is {}, I am {}'.format(name, age)
>>>
>>> print(content)
Hello, My name is john, I am 27
>>>
```

和字符串处理相关的，还有一个**join**方法。如果有多个变量，想拼接成一个字符串，且用竖线分隔，可以这样：
```
>>> name = 'john'
>>> address = 'Beijing'
>>> email = 'faker@mail.com'
>>>
>>> arr = [name, address, email]
>>> '|'.join(arr)
'john|Beijing|faker@mail.com'
>>>
```

### 变量处理相关
如何把数组中的多个元素赋值给多个变量，如果是以前，我可能会这样写：
```
>>> arr = ['john', 'Beijing', 'faker@mail.com']
>>>
>>> name = arr[0]
>>> address = arr[1]
>>> email = arr[2]
```
现在我会这样写：
```
>>> name, address, email = arr
```
如果你想跳过数组中的某个变量，可以用下划线：
```
>>> arr = ['john', 'Beijing', 'faker@mail.com']
>>> name, _, email = arr
```

### 字典默认值
假设有一个这样的场景，需要动态创建一个字典，每个key对应一个列表，如果key不存在则需要先创建这个key，再往key中append元素。常规写法：
```
>>> list
[['friuit', 'apple'], ['friuit', 'orange'], ['friuit', 'banana'], ['friuit', 'pear'], ['device', 'phone'], ['device', 'laptop'], ['device', 'mac']]
>>> data = {}
>>> for (k,v) in list:
...     if k not in data:
...             data[k] = []
...     data[k].append(v)
...
>>> data
{'friuit': ['apple', 'orange', 'banana', 'pear'], 'device': ['phone', 'laptop', 'mac']}
>>>
```
比较pythonic的写法：
```
>>> data = {}
>>> list
[['friuit', 'apple'], ['friuit', 'orange'], ['friuit', 'banana'], ['friuit', 'pear'], ['device', 'phone'], ['device', 'laptop'], ['device', 'mac']]
>>> 
>>> [data.setdefault(k, []).append(v) for (k,v) in list]
[None, None, None, None, None, None, None]
>>> data
{'friuit': ['apple', 'orange', 'banana', 'pear'], 'device': ['phone', 'laptop', 'mac']}
>>>
```

### 列表推导
举个例子，如何将一个文件中的每一行读取出来，并组成一个列表。如果是以前，我可能会这样写：
```
>>> filename = 'balance_query_input.txt'
>>> f = open(filename, encoding='utf-8')
>>> items = f.readlines()
>>> for item in items:
...     item = item.strip()
>>> f.close()
>>>
```
如果是现在，我会这样写：
```
>>> items = [x.strip() for x in open(filename, encoding='utf-8').readlines()]
```
如果想同时过滤掉文件中的空行和长度不足4的行，可以这样：
```
>>> items = [ x.strip() for x in open(filename, encoding='utf-8').readlines() if len(x.strip())>4 ]
>>>
```
如何对列表中的每个元素，调用一个方法？如果是以前，我可能会这样写：
```
>>> items = [ x.strip() for x in open(filename, encoding='utf-8').readlines() if len(x.strip())>4 ]
>>>
>>> for item in item:
...     deal(item)
...
```
现在我会这样写：
```
>>> [ deal(item) for item in items ]
```
总之，学会使用列表推导后，基本上不用写for循环了。

### 使用内置函数map
其实列表推导的效果，使用map同样能实现，但是据说由于map是内置函数，所以效率会高一些。举个例子，对一个列表中所有元素增加10，首先定义一个inc函数：
```
>>> arr = [x for x in range(10)]
>>>
>>> arr
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
>>>
>>> def inc(val):
...     return val + 10
...
```
先演示使用列表推导的实现方式：
```
>>> [inc(x) for x in arr]
[10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
>>>
```
再演示使用map的实现方式：
```
>>> [x for x in map(inc, arr)]
[10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
```

### 使用lambda表达式
lambda表达式的作用是，创建匿名对象。好处就是，不用单独定义函数了。使用lambda表达式对上面的代码再进一步简化：
```
>>> [x for x in map(lambda x:x+10, arr)]
[10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
>>>
```

### 使用sorted进行自定义排序
假设现在有一些文件，名称按照数字命名，如果使用sorted默认排序，将会按照字符串来处理，这并不是我们想要的结果：
```
>>> data
['1.PDF', '10.PDF', '11.PDF', '12.PDF', '13.PDF', '2.PDF', '3.PDF', '4.PDF', '5.PDF', '6.PDF', '7.PDF', '8.PDF', '9.PDF']
>>> sorted(data)
['1.PDF', '10.PDF', '11.PDF', '12.PDF', '13.PDF', '2.PDF', '3.PDF', '4.PDF', '5.PDF', '6.PDF', '7.PDF', '8.PDF', '9.PDF']
>>>
```
我们想按照数字大小进行排序，则需要使用sorted的自定义*key*参数：
```
>>> data
['1.PDF', '10.PDF', '11.PDF', '12.PDF', '13.PDF', '2.PDF', '3.PDF', '4.PDF', '5.PDF', '6.PDF', '7.PDF', '8.PDF', '9.PDF']
>>>
>>>
>>> sorted(data, key=lambda i:int(i.split('.')[0]))
['1.PDF', '2.PDF', '3.PDF', '4.PDF', '5.PDF', '6.PDF', '7.PDF', '8.PDF', '9.PDF', '10.PDF', '11.PDF', '12.PDF', '13.PDF']
>>>
```
参数key是一个函数，接收data中的每个元素，并返回一个值。

如果想对一个字典进行排序，也可以使用key进行处理，使得sorted很灵活：
```
>>> data
[{'key': '1'}, {'key': '2'}, {'key': '100'}, {'key': '55'}, {'key': '6'}]
>>>
>>> sorted(data, key=lambda i:int(i['key']))
[{'key': '1'}, {'key': '2'}, {'key': '6'}, {'key': '55'}, {'key': '100'}]
>>>
```

lambda表达式、map、filter、sorted这几个神器，还有很多高阶应用，欢迎各位读者补充。

### 使用多线程+队列完成可并行处理的任务
多线程+队列，是一种编程模型，被称为生产者/消费者模型。

假设有一个队列中保存着所有待处理任务，如果这些任务是可以并行处理互不干扰，则可以使用这种编程模型。

举个例子，要把所有URL中的html内容解析出结构化数据并存储起来。这种场景就可以使用生产者/消费者模型。
```
import queue
from threading import Thread
import threading

# 任务队列，存放待处理任务，可由多个线程并行处理
task_queue = queue.Queue()

# 结果队列，存放结构化数据，由单线程逐个获取并存储到数据库
result_queue = queue.Queue()

# 多线程任务逻辑，不停从任务队列中领取任务，并将处理结果塞入结果队列。
def do_deal():
    while True:
        item = ''
        try:
            item = task_queue.get_nowait()
        except queue.Empty as e:
            log("task queue is empty, sleep a while.")
            time.sleep(1)
            continue
        data = deal(item)
        result_queue.put(data)

# 单线程从结果队列获取结构化数据，并存储到数据库或者文件
def do_save():
    while True:
        record = ''
        try:
            record = result_queue.get_nowait()
        except queue.Empty as e:
            log("result queue is empty. sleep a while.")
            time.sleep(1)
            continue
        log('record:' + record)
        f = open(OUTPUT, 'a')
        f.write(record)
        f.close()

# 创建任务队列
tasks_list = [ x for x in filter(lambda x: True if x.split('/')[4]+x.split('/')[5] not in done_list else False, [ y.strip() for y in open(INPUT).readlines() ]) ]

[ task_queue.put(x) for x in tasks_list ]

# 启动多个任务处理线程
for i in range(6):
    Thread(target=do_deal, name='producer'+str(i)).start()

# 启动单个结果处理线程
Thread(target=do_save, name='consumer1').start()
```

## 参考学习

官方文档是最好的学习资料

[https://docs.python.org/zh-cn/3/](https://docs.python.org/zh-cn/3/)

## 推荐项目
一个用Python写的爬取豆瓣小组信息的项目：[https://gitee.com/mktime/scrapy-douban-group](https://gitee.com/mktime/scrapy-douban-group)

## 延申阅读
[数据分析为什么常用Jupyter而不是直接使用Python脚本或Excel?](https://www.zhihu.com/question/37490497)

[关乎Python lambda你也看得懂](https://zhuanlan.zhihu.com/p/56100507)

[python lambda结合列表推导式](https://www.zhihu.com/question/56193983)

[python3内置函数map](https://www.zhihu.com/column/p/28927667)

[怎样才能写出 Pythonic 的代码？](https://www.zhihu.com/question/21408921)

[Python 有哪些优雅的代码实现让自己的代码更pythonic？](https://www.zhihu.com/question/37751951)

[2020年10个不错的Python库](https://mp.weixin.qq.com/s/sxgQnQbnF4hiKgdJ9ifAmg)

[酷！一个仿漫画手绘风格的 Python 图表库](https://mp.weixin.qq.com/s/dJ7hn063PeN8c2GiYAWD6A)

[8k Star！基于 Matplotlib 的数据可视化利器](https://mp.weixin.qq.com/s/hanAZPV_GSefc9-89ydXDw)

[1.5 万 Star！大大提升终端体验的好工具：Rich](https://mp.weixin.qq.com/s/VKktUTRjwcf1A0bCg6jhiw)

[用Python实现定时自动化收取蚂蚁森林能量，再也不用担心忘记收取了](https://mp.weixin.qq.com/s/XvtI_CV8EHdO0Z-mmdIz1w)


## 案例
不定期更新

### wind_crawl.py
https://gitee.com/mktime/python-learn/blob/master/wind_crawl.py

desc: Linux下调用wind Python API 获取股票、债券、基金耵市数据，以及70个中大城市新建商品房/二手住房房价数据.

### test_pdf2images.py
https://gitee.com/mktime/python-learn/blob/master/test_pdf2images.py

desc: 将PDF拆分成图片。

### balance_query.py
https://gitee.com/mktime/python-learn/blob/master/balance_query.py

​    date: 2021-02-05 17:36:28

​    desc: 这是一个实际的工作例子，结合tcp socket、配置解析、文件读取、字符串处理的用法，实现一个完整功能。

​    link: [https://docs.python.org/zh-cn/3/library/configparser.html](https://docs.python.org/zh-cn/3/library/configparser.html)

### baidu_ocr.py
https://gitee.com/mktime/python-learn/blob/master/baidu_ocr.py

​    2021-01-27 16:38:38

​    desc: 百度OCR接口识别身份证程序，使用消费者/生产者模型。

​    ref: [https://cloud.baidu.com/doc/OCR/index.html](https://cloud.baidu.com/doc/OCR/index.html)

### print_ascii.py
https://gitee.com/mktime/python-learn/blob/master/print_ascii.py

​    date:2020-12-27 23:53:23

​    desc:有时候身边没有Linux环境，不能方便查看ascii码表，这个就能派上用场了。

### egcd.py
https://gitee.com/mktime/python-learn/blob/master/egcd.py

​    date:2020-12-27 23:48:40

​    desc:扩展欧几里得

​    ref: [https://baike.baidu.com/item/%E6%89%A9%E5%B1%95%E6%AC%A7%E5%87%A0%E9%87%8C%E5%BE%97%E7%AE%97%E6%B3%95/2029414](https://baike.baidu.com/item/%E6%89%A9%E5%B1%95%E6%AC%A7%E5%87%A0%E9%87%8C%E5%BE%97%E7%AE%97%E6%B3%95/2029414)

### learn_thread.py
https://gitee.com/mktime/python-learn/blob/master/learn_thread.py

​    date:2020-12-27 23:34:42

​    desc:学习使用线程池

### scan2.py
https://gitee.com/mktime/python-learn/blob/master/scan2.py

​    date:2020-12-27 23:33:33

​    desc:对多个IP地址的常见端口快速探测。

### scan.py
https://gitee.com/mktime/python-learn/blob/master/scan.py

​    date2020-12-27 23:32:48 

​    desc:对一个IP地址的所有端口快速探测。

### parse_excel.py
https://gitee.com/mktime/python-learn/blob/master/parse_excel.py

​    date:2020-08-26 11:05:29

​    desc:使用xlrd,xlwt实现excel文件的创建、修改，实现基于模板xls文件的文件创建，当模板xls文件带有格式时，能自动带入。

​    mail: withfaker at gmail.com

### flv2mp4.py
https://gitee.com/mktime/python-learn/blob/master/flv2mp4.py

​    date: 2020-02-13 15:24:21

​    desc: 将flv视频转换为mp4格式

​    mail: withfaker at gmail.com

​    link: [B站开源的纯HTML播放FLV视频](https://github.com/Bilibili/flv.js)

### test_img2pdf.py
https://gitee.com/mktime/python-learn/blob/master/test_img2pdf.py

​    date: 2020-02-26 00:08:50

​    desc: 将多张jpg图片合成一个PDF文件。

​    mail: withfaker at gmail.com

​    TLDR: 一个朋友发来一个PDF，要删除每张片子底部的链接文字，然后合成一个新的PDF。 搜了一下，免费的工具都是扯淡。只好打开PS，导入pdf文件，PS会自动生成多张图片，用橡皮擦工具擦除底部的链接，重新导出每张照片，然后用img2pdf这个库，就好了。

### xml2vcf.py
https://gitee.com/mktime/python-learn/blob/master/xml2vcf.py

​    data:   2014-02-09 01:23:51

​    desc:   将刷机精灵软件导出的xml格式联系人转换为vcf格式联系人

​    mail:   withfaker@gmail.com

​    blog:   http://mktime.org

​    0.使用前,先将刷机精灵导出的xml文件第一行删除.

​    1.如果导出的文件有空行,  :g/^$/d

​    2.如果导出的用户号码中间有横杠,   :%s/-//g

### mydict.py
https://gitee.com/mktime/python-learn/blob/master/mydict.py

​    date:   2014-02-27 11:48:04

​    desc:   百度/有道翻译接口使用

​    email:  withfaker@gmail.com

​    urls:   [http://fanyi.youdao.com/openapi](http://fanyi.youdao.com/openapi)

​    update: 2014-02-27 17:11:43
​            经过比对,百度翻译比有道翻译差的不只是一点点...

​            2014-09-04 10:29:19

​            经过测试，发现百度翻译已经翘辫子了，故从代码中剔除了对百度翻译接口的调用

​            网易是一家有态度的公司

### get_bookmarks.py
https://gitee.com/mktime/python-learn/blob/master/get_bookmarks.py

​    date    2013-07-20 18:39:01

​    desc    convert delicious xml file to bookmarkdown format

​    email:  withfaker@gmail.com

### fetchurl.py
https://gitee.com/mktime/python-learn/blob/master/fetchurl.py

​    date:   2014-03-03 18:13:33

​    url:    http://www.dbmeizi.com

​    desc:   fetch all images from dbmeiz.com

​    email:  withfaker@gmail.com

​    update: 没有使用thread,简单的循环处理,担心服务端的拒绝[403].

### learn-thread.py
https://gitee.com/mktime/python-learn/blob/master/learn-thread.py

​    date:   2014-03-07 20:51:03

​    url:    http://www.dbmeizi.com

​    desc:   fetch all images from dbmeiz.com

​    email:  withfaker@gmail.com

​    update: 使用多线程下载豆瓣妹子图片

### qiushi.py
https://gitee.com/mktime/python-learn/blob/master/qiushi.py

​    date:   2014-03-27 23:40:15

​    url:    http://www.qiushibaike.com
​    desc:   fetch some jokes from this site.

​    email:  withfaker@gmail.com

​    update: save content into mysql database.

### sock5.py
https://gitee.com/mktime/python-learn/blob/master/sock5.py

​    url:    http://xiaoxia.org/2011/03/29/written-by-python-socks5-server/ 

​    desc:   socks5 proxy server written using python

​    email:  withfaker@gmail.com

### data-structure.py
https://gitee.com/mktime/python-learn/blob/master/data-structure.py

​    date:   2014-07-10 16:26:48

​    desc:   数据结构、算法

​    email:  withfaker@gmail.com

### get_version.py
https://gitee.com/mktime/python-learn/blob/master/get_version.py

​    date:   2014-07-10 17:26:31

​    desc:   Windows电脑版本判断

​    email:  withfaker@gmail.com

### ipfind.py
https://gitee.com/mktime/python-learn/blob/master/ipfind.py

​    date:   2014-07-10 17:27:06

​    url:    [http://lilydjwg.is-programmer.com/2014/5/27/qqwry-dat-parser-python-3-version.47207.html](http://lilydjwg.is-programmer.com/2014/5/27/qqwry-dat-parser-python-3-version.47207.html)

​    desc:   纯真IP数据库QQWry解析库Python3版

​    email:  withfaker@gmail.com

### pm25.py
https://gitee.com/mktime/python-learn/blob/master/pm25.py

​    date:   2014-07-10 17:29:47

​    url:    http://pm25.in

​    desc:   pm2.5乌鲁木齐数据监测

​    email:  withfaker@gmail.com

### rpc_server.py
https://gitee.com/mktime/python-learn/blob/master/rpc_server.py

​    date:   2014-07-10 17:30:37

​    desc:   如何使用RPC server

​    email:  withfaker@gmail.com

### sock_client.py/sock_serv.py
https://gitee.com/mktime/python-learn/blob/master/sock_client.py

​    date:   2014-07-10 17:31:41

​    desc:   socket TCP example

​    email:  withfaker@gmail.com

### tray.py
https://gitee.com/mktime/python-learn/blob/master/tray.py

​    date:   2014-07-10 17:32:22

​    desc:   Windows平台系统通知栏系统图标的展示(windows system tray notify)

​    email:  withfaker@gmail.com

### smz_report.py
https://gitee.com/mktime/python-learn/blob/master/smz_report.py

​    date:   2014-09-04 10:24:17

​    desc:   使用python从数据库提取数据，然后发送HTML格式邮件

​    email:  withfaker@gmail.com

### import_ess_picture.py
https://gitee.com/mktime/python-learn/blob/master/import_ess_picture.py

​    date:   2014-09-04 10:25:15

​    desc:   使用python连接oracle，解析文本文档，更新数据库中clob字段内容

​    email:  withfaker@gmail.com

### export_delicious.sh
https://gitee.com/mktime/python-learn/blob/master/export_delicious.sh

​    date:   2014-10-05 16:25:11

​    desc:   导出delicious链接

### ve2x.py
https://gitee.com/mktime/python-learn/blob/master/ve2x.py

​    date:   2014-10-07 00:33:45

​    desc:   find v2ex top 8 item from v2ex mongo database crawled by v2ex spider

​    email:  withfaker@gmail.com

### remote.sh
https://gitee.com/mktime/python-learn/blob/master/remote.sh

​    date:   2015-02-23 22:10:12

​    desc:   一键连接openvpn并且以远程桌面的方式连接办公室的windows电脑

​    email:  withfaker@gmail.com

### monitor.sh
https://gitee.com/mktime/python-learn/blob/master/monitor.sh

​    date:   2015-03-03 10:35:27

​    desc:   关键进程监控脚本

​    email:  withfaker@gmail.com

### auto_build.py
https://gitee.com/mktime/python-learn/blob/master/auto_build.py

​    date:   2015-03-16 10:48:22

​    desc:   解析xls中的编译任务,执行程序的编译命令,将结果输出到xls中.

​    email:  withfaker@gmail.com

### rpc_server.py
https://gitee.com/mktime/python-learn/blob/master/rpc_server.py

​    date:   2015-03-17 11:57:36

​    desc:   简易的基于tcp socket的远程调用,客户端通过socket将需要执行的命令发给tcp server,tcp server执行后将执行结果返回给tcp client.

​    email:  withfaker@gmail.com

### http_proxy.py
https://gitee.com/mktime/python-learn/blob/master/http_proxy.py

​    date:   2015-04-15 11:33:13

​    desc:   simple http proxy

​    email:  fnds3000@gmail.com

### findbig.py
https://gitee.com/mktime/python-learn/blob/master/findbig.py

​    date:   2015-06-11 11:23:23

​    desc:   找出目录内的大文件

​    email:  withfaker@gmail.com

### find_repeat.py
https://gitee.com/mktime/python-learn/blob/master/find_repeat.py

​    date:   2015-07-16 12:56:27

​    author: withrock

​    mail:   withfaker@gmail.com

​    desc:   查找硬盘上的重复文件

​            this tool is used to find content-repeated file in a directory tree. 

​            it didn't not use md5 hash because of slowly for calcluating big file.

​            i wrote a simple hash function `calc_hash` whhich just depends on file size and file name.

​            if you have good idea for quickly calcluating file hash, 

​            you can give me some advise, thank you!

### screen.sh
https://gitee.com/mktime/python-learn/blob/master/screen.sh

​    date:    2015-11-04 11:13:51

​    author:  withrock

​    mail:    faker@163.com

​    desc:    一个简单的脚本，方便用来调整笔记本的显示屏亮度。结合awesome wm使用会更加方便。

​           通过alt+F5, alt+F6快捷调整显示屏亮度。

### wifi-keep.sh
https://gitee.com/mktime/python-learn/blob/master/wifi-keep.sh

​    date:   2015-11-10 19:23:56

​    author: withrock

​    mail:   faker@163.com

​    desc:   一个在网络不稳定的情况下，能自动连接wifi的小脚本。需要借助netctl。运行时需要root权限(sudo ./wifi-keep.sh)



