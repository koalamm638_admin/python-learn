import fitz
import os

'''
pip install fitz
pip install PyMuPDF
'''

# 调整zoom_x/zoom_y 可以调整图片清晰度
def pdf_image(pdfPath,imgPath,zoom_x,zoom_y,rotation_angle):
    pdf = fitz.open(pdfPath)
    for pg in range(0, pdf.pageCount):
        page = pdf[pg]
        trans = fitz.Matrix(zoom_x, zoom_y).preRotate(rotation_angle)
        pm = page.getPixmap(matrix=trans, alpha=False)
        pm.writePNG(imgPath+str(pdfPath)+"_" + str(pg) + ".png")
    pdf.close()

data = [x for x in os.listdir('.') if x.endswith('.PDF')]
for i in data:
    print(i)
    pdf_image(i, "..\\output\\",1,1,0)


