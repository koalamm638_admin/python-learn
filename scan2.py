from socket import *
import threading
import queue

threads = []
q = queue.Queue()
for i in range(1, 255):
    ip_addr = '10.7.67.' + str(i)
    for port in [21, 22, 20, 80, 8080, 8081, 3389, 443, 445, 3306, 445]:
        q.put((ip_addr, port))

def portScanner(q):
    while True:
        addr = None
        try:
            addr = q.get_nowait()
        except queue.Empty:
            print("finished")
            break
        #print('[-] [%s] %d scanning' % (addr[0], addr[1]))
        try:
            s = socket(AF_INET,SOCK_STREAM)
            s.connect((addr[0], addr[1]))
            print('[+] [%s] %d open!!!' % (addr[0], addr[1]))
            s.close()
        except:
            continue

def main():
    setdefaulttimeout(2)
    thread_num = 50
    for i in range(thread_num):
        args = {'q': q}
        t = threading.Thread(target=portScanner, kwargs=args)
        threads.append(t)
        t.start()
    for t in threads:
        t.join()
    print('[*] The scan is complete!')

if __name__ == '__main__':
    main()
