from socket import *
import threading
import queue

threads = []
q = queue.Queue()
for i in range(1, 65535):
    q.put(i)

def portScanner(host, q):
    while True:
        port = -1
        try:
            port = q.get_nowait()
        except queue.Empty:
            print("finished")
            break
        #print(port)
        if port == -1:
            break
        try:
            s = socket(AF_INET,SOCK_STREAM)
            s.connect((host,port))
            print('[+] %d open' % port)
            s.close()
        except:
            pass

def main():
    setdefaulttimeout(2)
    host = '47.112.137.92'
    thread_num = 100
    for i in range(thread_num):
        t = threading.Thread(target=portScanner, args=(host, q))
        threads.append(t)
        t.start()
    for t in threads:
        t.join()
    print('[*] The scan is complete!')

if __name__ == '__main__':
    main()
