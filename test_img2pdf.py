import img2pdf
import os
img_list = [x for x in os.listdir('.') if x.endswith('.png')]
img_list = sorted(img_list, key=lambda i: int(i.split('.')[0]))
print(img_list)

with open('new.pdf','wb') as f:
    f.write(img2pdf.convert(img_list))
